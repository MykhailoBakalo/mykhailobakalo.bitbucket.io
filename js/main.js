;(function ($) {

  $(document).ready(function() {

    // Open save search popup
    $('.js-ss-button').on('click', function() {
      $('#create-search-alert').addClass('sa-ss-pop--active');
    });

    $('.sa-back-to-search-alert').on('click', function() {
      $('#continue-with-email').removeClass('sa-ss-pop--active');
      $('#create-search-alert').addClass('sa-ss-pop--active');
    });

    // Close current opened popup
    $('.sa-ss-pop__cross').on('click', function() {
      $(this).closest('.sa-ss-pop').removeClass('sa-ss-pop--active');
    });

    // Open register with email popup
    $('#sa-ss-email').on('click', function(e) {
      e.preventDefault();
      $('.sa-ss-pop').removeClass('sa-ss-pop--active');
      $('#continue-with-email').addClass('sa-ss-pop--active');
    });

    // Open subscribe to search alerts

    $('#sa-signup-submit').on('click', function(e) {
      e.preventDefault();
      $('#continue-with-email').removeClass('sa-ss-pop--active');
      $('#sa-subscribe').addClass('sa-ss-pop--active');
    });

    // Skip subscribe to search alerts
    $('#sa-subscribe-skip').on('click', function() {
      // $('.sa-search-filters').addClass('sa-search-filters--active');
      $('#sa-subscribe').removeClass('sa-ss-pop--active');
    });

    // Submit subscribe to search alerts
    // $('#sa-subscribe-submit').on('click', function() {
    //   $('.sa-search-filters').addClass('sa-search-filters--active');
    //   $('#sa-subscribe').removeClass('sa-ss-pop--active');
    // });
    $('#sa-change-search-criteria').on('click', function() {
      $('.sa-search-filters').addClass('sa-search-filters--active');
      $('#create-search-alert').removeClass('sa-ss-pop--active');
    });

    $('.sa-search-filters__cross, #sa-search-final-cancel, #sa-subscribe-submit').on('click', function() {
      $('.sa-search-filters').removeClass('sa-search-filters--active');
      $('#create-search-alert').addClass('sa-ss-pop--active');
    });

    // Validate notification subscribtion form
    if($('#sa-subscribe-form').length) {
      let formValid = false;
      const phoneNotify = $('#sa-phone');
      const inputNotify = $('.sa-radio__input');

      phoneNotify.on('input change', function () {
        if(phoneNotify.val() == '' || phoneNotify.val().length < 9) {
          formValid = false;
          phoneNotify.addClass('sa-input--invalid');
          phoneNotify.next('.sa-validation-message').slideDown();
        } else {
          phoneNotify.removeClass('sa-input--invalid');
          phoneNotify.next('.sa-validation-message').slideUp();

          if($('#sms-immediately').is(':checked') || $('#sms-daily').is(':checked')) {
            formValid = true;
          }
        }

        if(formValid) {
          $('#sa-subscribe-submit').attr('disabled', false);
        } else {
          $('#sa-subscribe-submit').attr('disabled', true);
        }
      });

      inputNotify.on('change', function () {
        if($('#email-immediately').is(":checked") || $('#email-daily').is(":checked")) {
          formValid = true; 
        } else {
          formValid = false;
        }

        if($('#sms-immediately').is(':checked') || $('#sms-daily').is(':checked')) {
          if(phoneNotify.val() == '' || phoneNotify.val().length < 9) {
            formValid = false;
            phoneNotify.addClass('sa-input--invalid');
            phoneNotify.next('.sa-validation-message').slideDown();
          } else {
            formValid = true;
            phoneNotify.removeClass('sa-input--invalid');
            phoneNotify.next('.sa-validation-message').slideUp();
          }
        }

        if(formValid) {
          $('#sa-subscribe-submit').attr('disabled', false);
        } else {
          $('#sa-subscribe-submit').attr('disabled', true);
        }
      });
    }

    // Save search signup validation
    const form = $('#save-search-signup');

    // lets check if the form exists
    if(form.length) {
      const email = $('#sa-email');
      const name = $('#sa-first-name');
      const surname = $('#sa-last-name');
      const password = $('#sa-password');
      const passwordRepeat = $('#sa-repeat-password');
      let regEx = /^[A-Z0-9_'%=+!`#~$*?^{}&|-]+([\.][A-Z0-9_'%=+!`#~$*?^{}&|-]+)*@[A-Z0-9-]+(\.[A-Z0-9-]+)+$/i;
      let validForm = true;
      let validEmail = false;
      let validPassword = false;
      let emailVal = email.val();
      let nameVal = name.val();
      let surnameVal = surname.val();


      $('#sa-password, #sa-repeat-password').on('change input', function (){
        let passwordVal = password.val();
        let passwordRepeatVal = passwordRepeat.val();        

        if (passwordVal.length > 8) {
          if (passwordVal != passwordRepeatVal) {
            validPassword = false;
            password.addClass('sa-input--invalid');
            passwordRepeat.addClass('sa-input--invalid');
            passwordRepeat.closest('.sa-input-wrap').find('.sa-vm-1').slideUp();
            passwordRepeat.closest('.sa-input-wrap').find('.sa-vm-2').slideDown();
          } else {
            validPassword = true;
            password.removeClass('sa-input--invalid');
            passwordRepeat.removeClass('sa-input--invalid');
            passwordRepeat.closest('.sa-input-wrap').find('.sa-vm-2').slideUp();
          }
        } else {
          validPassword = false;
          password.addClass('sa-input--invalid');
          passwordRepeat.addClass('sa-input--invalid');
          passwordRepeat.closest('.sa-input-wrap').find('.sa-vm-1').slideDown();
        }
      });

      email.on('input change', function() {
        emailVal = email.val();
        validEmail = regEx.test(emailVal);

        if (!validEmail) {
          email.addClass('sa-input--invalid');
          email.next('.sa-validation-message').slideDown();
        } else {
          email.removeClass('sa-input--invalid');
          email.next('.sa-validation-message').slideUp();
        }
      });

      name.on('input change', function () {
        nameVal = name.val();

        if (nameVal.length < 2) {
          name.addClass('sa-input--invalid');
          name.next('.sa-validation-message').slideDown();
        } else {
          name.removeClass('sa-input--invalid');
          name.next('.sa-validation-message').slideUp();
        }
      });

      surname.on('input change', function () {
        surnameVal = surname.val();
  
        if(surnameVal.length < 2) {
          surname.addClass('sa-input--invalid');
          surname.next('.sa-validation-message').slideDown();
        } else {
          surname.removeClass('sa-input--invalid');
          surname.next('.sa-validation-message').slideUp();
        }
      });


      form.find('.sa-input').on('input', function() {

        if (nameVal.length < 2 || surnameVal.length < 2 || !validEmail ||  !validPassword) {
          validForm = false;
        } else {
          validForm = true;
        }

        if(validForm) {
          form.find('.sa-button-submit').attr('disabled', false);
        } else {
          form.find('.sa-button-submit').attr('disabled', true);
        }

        console.log('valid form', validForm);
        console.log('valid email', validEmail);
        console.log('valid passw', validPassword);
      });
    }

    $('.init-datepicker').datepicker({
      altField: "#actualDate"
    });

    if($(".js-range-slider-rent").length) {
      const rangeSliderRent = $(".js-range-slider-rent");
      const fromRangeRent = $('.js-from-monthy-rent .value');
      const toRangeRent = $('.js-to-monthy-rent .value');

      rangeSliderRent.ionRangeSlider({
        skin: "round",
        type: "double",
        hide_min_max: true,
        hide_from_to: true,
        onStart: updateRentInputs,
        onChange: updateRentInputs,
        onFinish: updateRentInputs
      });

      function updateRentInputs (data) {
        let from = data.from_pretty;
        let to = data.to_pretty;
    
        fromRangeRent.text(from);
        toRangeRent.text(to);
      }
    }

    if($(".js-range-slider-room").length) {
      const rangeSliderRoom = $(".js-range-slider-room");
      const fromRangeRoom = $('.js-from-room .value');
      const toRangeRoom = $('.js-to-room .value');

      rangeSliderRoom.ionRangeSlider({
        skin: "round",
        type: "double",
        hide_min_max: true,
        hide_from_to: true,
        onStart: updateRoomInputs,
        onChange: updateRoomInputs,
        onFinish: updateRoomInputs
      });
    
  
      function updateRoomInputs (data) {
        let from = data.from_pretty;
        let to = data.to_pretty;
    
        fromRangeRoom.text(from);
        toRangeRoom.text(to);
      }
    }

    // Search filters validation before save
    if($('.sa-search-filters')) {
      let startDate = '';
      let endDate = '';
      let noEndDate = false;
      let houseType = false;
      const submitButton = $('#sa-search-final-submit'); 

      $('#sa-start-date').on('input change', function() {
        startDate = $(this).val();

        if($('#sa-no-end-date').is(':checked')) {
          noEndDate = true;
        } else {
          noEndDate = false;
        }
      });

      $('#sa-end-date').on('input change', function() {
        endDate = $(this).val();
      });

      $('#type-appartments, #type-house-villa, #type-room').on('change', function() {
        if($('#type-appartments').is(':checked') || $('#type-house-villa').is(':checked') || $('#type-room').is(':checked')) {
          houseType = true;
        } else {
          houseType = false;
        }
      });

      $('#sa-no-end-date').on('change', function () {
        if($(this).is(':checked')) {
          noEndDate = true;
        } else {
          noEndDate = false;
        }
      });

      $('#type-appartments, #type-house-villa, #type-room, #sa-no-end-date, #sa-end-date, #sa-start-date').on('input change', function () {
        if(startDate != '' && endDate != '' && houseType) {
          submitButton.attr('disabled', false);
        } else if(noEndDate && startDate != '' && houseType) {
          submitButton.attr('disabled', false);
        } else {
          submitButton.attr('disabled', true);
        }
      });

      if ($("#sa-city-area").length) {
        var instance = $("#sa-city-area").autocomplete({
            autoFocus: true,
            delay: 200, minLength: 3, appendTo: $(".sa-search-place-results"),
            source: function (request, response) {
                $.ajax({
                    //todo: live:
                    url: "/Home/GetPredictionsJson",
                    // url: "kis.json",
                    data: {searchText: request.term},
                    type: "get",
                    dataType: "json",
                    crossDomain: true,
                    success: function (data) {
                        if (data.length == 0) {
                            return false;
                        } else {
                            response($.map(data, function (item) {
                                return {
                                    label: item.description,
                                    value: item.description,
                                    latitude: item.latitude,
                                    longitude: item.longitude
                                }
                            }));
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(thrownError);
                    }
                });
            },
            select: function (event, ui) {
                var item = ui.item;
                var itemLabel = item.label;
                var addressValue = itemLabel.replace(', Sverige', '');
                addressValue = addressValue.replace(', Stockholm', '');

                $('.js-address-value').text(addressValue);
                $('#sa-city-area').val(item.label);
                // $('#js-samtrygg-place').val(item.label);

                console.log(item.latitude);
                console.log(item.longitude);

                event.preventDefault();
            }
        }).autocomplete("instance");
        instance._renderItem = function (ul, item) {
            $(ul).addClass("sa-search-place-results");
            return $("<a href=\"javascript:\"><li>" + $("<span class=\"location2\"></span>").text(item.label).prop("outerHTML") + "</li></a>").appendTo(ul);
        };
      }
    }

  });

})(jQuery);